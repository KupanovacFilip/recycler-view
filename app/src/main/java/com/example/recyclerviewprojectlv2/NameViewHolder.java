package com.example.recyclerviewprojectlv2;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NameViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView textView;
    private NameClickListener clickListener;
    private ImageButton imgButton;

    public NameViewHolder(@NonNull View itemView, NameClickListener listener) {
        super(itemView);
        this.clickListener = listener;
        textView=itemView.findViewById(R.id.tvIme);
        imgButton=itemView.findViewById(R.id.imgButton);
        imgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onButtonClick(getAdapterPosition());
            }
        });
    }

    public void setName(String name){
        textView.setText(name);
    }
    @Override
    public void onClick(View v) {
        clickListener.onNameClick(getAdapterPosition());
    }
}
