package com.example.recyclerviewprojectlv2;

public interface NameClickListener {
    void onNameClick(int position);
    void onButtonClick(int position);
}
