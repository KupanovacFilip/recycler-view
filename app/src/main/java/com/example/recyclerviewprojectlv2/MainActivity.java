package com.example.recyclerviewprojectlv2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NameClickListener{
    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;
    private EditText etStudent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupRecycler();
        setupRecyclerData();
        etStudent=findViewById(R.id.etUnosImena);
    }

    private void setupRecyclerData() {
        List<String> data = new ArrayList<>();
        adapter.addData(data);
    }

    private void setupRecycler()
    {
        recyclerView=findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RecyclerAdapter(this);
        recyclerView.setAdapter(adapter);
    }
    public void addCell(View view){
        String name=etStudent.getText().toString();
        if(name.isEmpty()==false)
            adapter.addNewCell(name, adapter.getItemCount());
    }

    @Override
    public void onNameClick(int position) {
        Toast.makeText(this, "Position: "+position, Toast.LENGTH_SHORT).show();
    }
    public void onButtonClick(int position){
        adapter.removeCell(position);
    }
}